import React, {useState, useEffect} from 'react';
import {AsyncStorage} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
// we would need to manually link this: https://react-native-async-storage.github.io/async-storage/docs/install/
// but this is the newer version and removes the warning
//import AsyncStorage from '@react-native-async-storage/async-storage';
import uuid from 'react-native-uuid';
import Home from './src/components/home/Home';
import {API_URL, JSON_RESPONSE} from './src/util/api-helpers';

const useConstructor = (callBack = () => {}) => {
  const [hasBeenCalled, setHasBeenCalled] = useState(false);
  if (hasBeenCalled) {
    return;
  }
  callBack();
  setHasBeenCalled(true);
};

const App = () => {
  const [deviceId, setDeviceId] = useState('');

  const clearAsyncStorage = async () => {
    AsyncStorage.clear();
  };

  // enable this for testing registration, since asyncStorage cannot be wiped otherwise it seems
  //clearAsyncStorage();

  const storeData = async (key: any, value: any) => {
    try {
      await AsyncStorage.setItem(key, value);
    } catch (error) {
      console.log(error);
    }
  };

  const retrieveData = async (key: any) => {
    try {
      return await AsyncStorage.getItem(key);
    } catch (error) {
      console.log(error);
    }
  };

  const registerDevice = async (deviceId: string) => {
    try {
      const response = await fetch(`${API_URL}/auth/register/device`, {
        method: 'POST',
        headers: JSON_RESPONSE,
        body: JSON.stringify({
          identifier: deviceId,
        }),
      });
      const status = await response.status;
      if (status == 200) {
        setDeviceId(deviceId);
      } else {
        console.log(response);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const initialiseApp = () => {
    // check, if we already have a device Id, if not, create one and register the device
    try {
      retrieveData('deviceId')
        .then(value => {
          if (value !== null) {
            const deviceId = value || '';
            console.log('found deviceId ' + deviceId);
            setDeviceId(deviceId);
          } else {
            const deviceId: any = uuid.v4();
            console.log('no deviceId exists, creating new', deviceId);
            storeData('deviceId', deviceId);

            console.log('registering device');
            registerDevice(deviceId);
          }
        })
        .catch(e => {
          console.log(e);
        });
    } catch (error) {
      console.log(error);
    }
  };

  useConstructor(() => {
    initialiseApp();
  });

  useEffect(() => {
    setDeviceId(deviceId);
  }, [deviceId]);

  useEffect(() => {
    SplashScreen.hide();
  });

  return <Home deviceId={deviceId} />;
};

export default App;
