import React, {useRef, useState} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import ThemeColor from '../../../theme/themeColor';
import {Calendar} from 'react-native-calendars';
import Moment from 'moment';
import XDate from 'xdate';
import {isSameDay} from '../../../util/moment-helpers';

const CalendarScreen = props => {
  let [startOfMonth, setStartOfMonth] = useState(Moment().startOf('month'));
  const calendarRef = useRef<Calendar>(null);

  const isLeftDisabled = (): boolean => {
    return startOfMonth.isBefore(Moment(props.startDate));
  };

  const isRightDisabled = (): boolean => {
    return Moment(startOfMonth).add(1, 'month').isAfter(Moment());
  };

  const getMarkedDates = () => {
    const newMarkedDates: {[key: string]: any} = {};
    const startDate = Moment(props.startDate).startOf('day');
    let currentDay = Moment(startOfMonth);
    const endOfCurrentMonth = Moment(currentDay).endOf('month');
    while (currentDay.isBefore(endOfCurrentMonth)) {
      const id = currentDay.format('YYYY-MM-DD');
      const isBeforeStartDate = currentDay.isBefore(startDate);
      const isInFuture = currentDay.isAfter(Moment());
      const mfd = props.mfds.find((m: any) =>
        isSameDay(Moment(m.mfdDate), currentDay),
      );
      const isMfd = !isInFuture && !!mfd;
      newMarkedDates[id] = {
        // Used to indicate if the day should be disabled
        disabled: isBeforeStartDate || isInFuture,
        // Used to indicate if it's a MFD
        selected: isMfd,
      };
      currentDay = Moment(currentDay).add(1, 'day');
    }
    return newMarkedDates;
  };

  const renderArrow = (direction: 'left' | 'right') => {
    let imageUri;
    if (direction === 'left') {
      imageUri = isLeftDisabled()
        ? require('./left-arrow-disabled.png')
        : require('./left-arrow.png');
    } else {
      imageUri = isRightDisabled()
        ? require('./right-arrow-disabled.png')
        : require('./right-arrow.png');
    }
    return (
      <Image
        style={{width: 16, height: 16}}
        source={imageUri}
        width={16}
        height={16}
      />
    );
  };

  const backMonth = () => {
    if (isLeftDisabled()) {
      // The user can't go back a month
      return;
    }
    if (calendarRef?.current) {
      const prev = Moment(startOfMonth).subtract(1, 'month');
      const date = new XDate(prev.get('year'), prev.get('month'), 1);
      // console.log('----------------------------------------------------------');
      // console.log(prev.get('year'), prev.get('month') + 1, 1);
      // console.log(date);
      // console.log('----------------------------------------------------------');
      calendarRef.current.updateMonth(date, true);
      setStartOfMonth(Moment(prev));
    }
  };

  const nextMonth = () => {
    if (isRightDisabled()) {
      // The user can't go forward a month
      return;
    }
    if (calendarRef?.current) {
      const next = Moment(startOfMonth).add(1, 'month');
      const date = new XDate(next.get('year'), next.get('month'), 1);
      // console.log('----------------------------------------------------------');
      // console.log(next.get('year'), next.get('month') + 1, 1);
      // console.log(date);
      // console.log('----------------------------------------------------------');
      calendarRef.current.updateMonth(date, true);
      setStartOfMonth(Moment(next));
    }
  };

  const renderHeader = () => {
    const arrowWrapperStyles = {
      width: 24,
      height: 24,
      justifyContent: 'center',
      alignItems: 'center',
    };
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginVertical: 32,
        }}>
        <TouchableOpacity
          style={arrowWrapperStyles}
          onPress={() => backMonth()}>
          {renderArrow('left')}
        </TouchableOpacity>
        <Text
          style={{color: ThemeColor.green, fontFamily: 'Rubik', fontSize: 20}}>
          {startOfMonth.format('MMMM')}
        </Text>
        <TouchableOpacity
          style={arrowWrapperStyles}
          onPress={() => nextMonth()}>
          {renderArrow('right')}
        </TouchableOpacity>
      </View>
    );
  };

  const renderDay = (dateData: any, marking: any) => {
    const textStyle = {
      textAlign: 'center',
      fontSize: 16,
      color: marking?.disabled
        ? '#BDBDBD'
        : marking?.selected
        ? '#fff'
        : '#333',
      fontFamily: marking?.disabled ? 'Rubik' : 'Rubik-SemiBold',
    };
    const moment = Moment(dateData.dateString, 'YYYY-MM-DD');
    const userStateDate = Moment(props.startDate).startOf('day');
    const endOfToday = Moment().endOf('day');
    const canEdit = moment.isBetween(userStateDate, endOfToday, null, '[]');

    const dateText = (
      <View
        style={{
          height: 40,
          width: 40,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={textStyle}>{dateData.day}</Text>
      </View>
    );
    const mfd = (
      <ImageBackground
        source={heartUri}
        style={{height: 40, width: 40, justifyContent: 'center'}}
        height={40}
        width={40}>
        {dateText}
      </ImageBackground>
    );
    const editableDate = (
      <TouchableWithoutFeedback
        style={{height: 40, width: 40}}
        onPress={() => props.toggleMfd(moment)}>
        <View
          style={{
            height: 40,
            width: 40,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {marking?.selected ? mfd : dateText}
        </View>
      </TouchableWithoutFeedback>
    );
    return canEdit ? editableDate : dateText;
  };

  return (
    <ImageBackground style={{flex: 1}} source={require('./picnic.jpg')}>
      <ScrollView>
        <View style={styles.container}>
          <Text
            style={{
              fontFamily: 'Rubik',
              fontSize: 24,
              color: '#ffffff',
              textAlign: 'center',
              marginBottom: 10,
            }}>
            {startOfMonth.format('yyyy')}
          </Text>
          <Calendar
            ref={calendarRef}
            firstDay={1}
            theme={calendarTheme}
            hideExtraDays={true}
            renderHeader={() => renderHeader()}
            hideArrows={true}
            dayComponent={({date, marking}: any) => renderDay(date, marking)}
            markedDates={getMarkedDates()}
          />
        </View>
      </ScrollView>
    </ImageBackground>
  );
};

const heartUri = require('./heart.png');

const calendarTheme = {
  textDayHeaderFontFamily: 'Rubik',
  textDayHeaderColor: ThemeColor.grey,
  textDayHeaderFontSize: 13,
  // Override the calendars stylesheet
  stylesheet: {
    calendar: {
      main: {
        container: {
          paddingHorizontal: 5,
          paddingBottom: 22,
          backgroundColor: '#fff',
        },
        week: {
          marginTop: 0,
          marginBottom: 0,
          flexDirection: 'row',
          justifyContent: 'space-around',
        },
      },
    },
  },
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 32,
    paddingHorizontal: 10,
  },
  monthLabel: {
    color: ThemeColor.green,
  },
});

export default CalendarScreen;
