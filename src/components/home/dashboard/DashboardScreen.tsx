import React, {useRef} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeColor from '../../../theme/themeColor';
import Moment from 'moment';

const DashboardScreen = (props: any) => {
  const scrollViewRef = useRef<ScrollView>(null);

  const renderWeek = (weekNum: number, numMfd: number) => {
    const isCurrent = props.currentWeek == weekNum - 1;
    let imageUri;
    switch (numMfd) {
      case 1: imageUri = require('./week-mfd-1.png'); break;
      case 2: imageUri = require('./week-mfd-2.png'); break;
      case 3: imageUri = require('./week-mfd-3.png'); break;
      case 4: imageUri = require('./week-mfd-4.png'); break;
      case 5: imageUri = require('./week-mfd-5.png'); break;
      case 6: imageUri = require('./week-mfd-6.png'); break;
      case 7: imageUri = require('./week-mfd-7.png'); break;
      default: imageUri = require('./week-mfd-0.png');
    }

    return (
      <View key={weekNum} style={{alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: 'Rubik',
            fontSize: 13,
            color: ThemeColor.grey,
            marginBottom: 10,
            fontWeight: isCurrent ? 'bold' : 'normal',
            textDecorationLine: isCurrent ? 'underline' : 'none',
          }}>
          WK {weekNum}
        </Text>
        <Image source={imageUri} width={25} height={160} />
      </View>
    );
  };

  const onPress = () => {
    props.toggleMfd(null);
    // also scroll to bottom for smaller screens so hearts added at the bottom are visible
    if (scrollViewRef?.current) {
      scrollViewRef.current.scrollToEnd({animated: true});
    }
  };

  function renderWeeks() {
    const countsByWeek = [0, 0, 0, 0, 0];
    for (let weekOffset = 0; weekOffset < countsByWeek.length; weekOffset++) {
      const startOfCurrWeek = Moment(props.startDate)
        .startOf('day')
        .add(weekOffset, 'weeks');
      const endOfCurrWeek = Moment(startOfCurrWeek)
        .add(weekOffset + 1, 'weeks')
        .subtract(1, 'day') // 7th day non-inclusive
        .endOf('day'); // Include all MFDs up to 11:59:59.9999
      // Get a list of MFDs in the desired week
      const ifMfdIsInWeek = (mfd: any) => {
        const d = Moment(mfd.mfdDate);
        return d.isBetween(startOfCurrWeek, endOfCurrWeek, null, '[]');
      };
      const mfds = props.mfds.filter(ifMfdIsInWeek);
      const tempMfds = props.tempMfds.filter(ifMfdIsInWeek);
      countsByWeek[weekOffset] = mfds.length + tempMfds.length;
    }
    let i = 1;
    return countsByWeek.map(count => renderWeek(i++, count));
  }

  const getStartOfWeek = () => {
    const currWeek = Moment(props.startDate).add(props.currentWeek, 'week');
    return currWeek.format('dddd DD MMM');
  };
  const getEndOfWeek = () => {
    const currWeek = Moment(props.startDate)
      .add(props.currentWeek, 'week')
      .add(6, 'days');
    return currWeek.format('dddd DD MMM');
  };

  const renderCallToAction = () => {
    const imageUri = props.addedMfdToday
      ? require('./heart-empty.png')
      : require('./heart-full.png');
    const image = <Image source={imageUri} width={56} height={51} />;
    if (props.addedMfdToday) {
      return image;
    } else {
      return (
        <TouchableOpacity onPress={onPress}>
          {image}
        </TouchableOpacity>
      )
    }
  };

  return (
    <View style={styles.container}>
      <ScrollView
        ref={scrollViewRef}
        contentContainerStyle={{backgroundColor: ThemeColor.deepGreen}}>
        <View style={styles.upper}>
          <Text style={[styles.text, styles.textLarge]}>My 1st Challenge</Text>
          <Text style={[styles.text, styles.textSmall]}>
            <Text>
              Between {getStartOfWeek()} and {getEndOfWeek()}, I'm determined to
              have {props.numMfdPerWeek}
            </Text>{' '}
            <Text style={styles.bold}>
              meat-free day{props.numMfdPerWeek > 1 ? 's' : ''}
            </Text>{' '}
            <Text>per week.</Text>
          </Text>
          <ImageBackground
            source={require('./trees.png')}
            imageStyle={styles.image}
            style={styles.background}>
            <Text style={styles.text}>My meat-free days</Text>
          </ImageBackground>
        </View>
        <View style={styles.calendarWrapper}>
          <View style={styles.calendarCta}>
            <Text style={styles.calendarCount}>
              {props.mfds?.length + props.tempMfds?.length}
            </Text>
            {renderCallToAction()}
          </View>
          <Text style={styles.breakdownHeading}>My weekly snapshot</Text>
          <View style={styles.breakdownWrapper}>{renderWeeks()}</View>
        </View>
      </ScrollView>
    </View>
  );
};

const calendarCtaHeight = 60;
const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#fff',
  },
  upper: {
    flex: 0,
    paddingTop: 22,
    paddingLeft: 25,
    paddingRight: 25,
  },
  text: {
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
    textShadowColor: '#000',
    textShadowOffset: {width: -1, height: 0},
    fontFamily: 'Rubik',
  },
  textLarge: {
    fontSize: 20,
  },
  textSmall: {
    marginTop: 15,
    fontSize: 13,
  },
  background: {
    marginTop: 12,
    height: 133,
    flexDirection: 'column-reverse',
    paddingBottom: calendarCtaHeight / 2 + 10,
  },
  image: {
    resizeMode: 'contain',
  },
  calendarWrapper: {
    position: 'relative',
    zIndex: 0,
    paddingTop: calendarCtaHeight / 2 + 22,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    paddingBottom: 16,
  },
  breakdownHeading: {
    fontSize: 16,
    color: '#333333',
    fontFamily: 'Rubik',
  },
  calendarCta: {
    position: 'absolute',
    backgroundColor: '#fff',
    left: 10,
    right: 10,
    top: (calendarCtaHeight / 2) * -1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    height: calendarCtaHeight,
    borderRadius: calendarCtaHeight / 2,
    paddingVertical: 6,
    paddingHorizontal: 12,
    borderColor: ThemeColor.border,
    borderWidth: 1,
    zIndex: 100,
  },
  calendarCount: {
    fontSize: 48,
    lineHeight: 51,
    paddingLeft: 8,
    fontFamily: 'Rubik-Light',
  },
  bold: {
    fontFamily: 'Rubik-SemiBold',
  },
  breakdownWrapper: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    paddingHorizontal: 50,
  },
});

export default DashboardScreen;
