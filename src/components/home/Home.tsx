import React, {useState, useEffect} from 'react';
import {Image, StatusBar, Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import DashboardScreen from './dashboard/DashboardScreen';
import ThemeColor from '../../theme/themeColor';
import CalendarScreen from './calendar/CalendarScreen';
import {API_URL, JSON_RESPONSE} from '../../util/api-helpers';
import Moment from 'moment';
import {isSameDay} from '../../util/moment-helpers';

function MoreScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>More goes here!</Text>
    </View>
  );
}

const Tab = createBottomTabNavigator();

const MyTabs = (props: any) => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Dashboard"
        children={() => {
          return (
            <DashboardScreen
              toggleMfd={(date) => props.toggleMfd(date)}
              mfds={props.mfds}
              tempMfds={props.tempMfds}
              numMfdPerWeek={props.numMfdPerWeek}
              addedMfdToday={props.addMfdToday}
              currentWeek={props.currentWeek}
              deviceId={props.deviceId}
              startDate={props.startDate}
            />
          );
        }}
        options={{
          tabBarActiveTintColor: ThemeColor.deepGreen,
          tabBarLabelStyle: {fontFamily: 'Rubik-SemiBold', fontSize: 10},
          tabBarIcon: ({focused}) => (
            <Image
              style={{maxHeight: 21, maxWidth: 25}}
              source={
                focused
                  ? require('./heart-active.png')
                  : require('./heart-inactive.png')
              }
              height={21}
              width={25}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Calendar"
        children={() => {
          return (
            <CalendarScreen
              startDate={props.startDate}
              mfds={props.mfds}
              tempMfds={props.tempMfds}
              toggleMfd={(date) => props.toggleMfd(date)}
            />
          );
        }}
        options={{
          tabBarActiveTintColor: ThemeColor.deepGreen,
          tabBarLabelStyle: {fontFamily: 'Rubik-SemiBold', fontSize: 10},
          tabBarIcon: ({focused}) => (
            <Image
              style={{maxHeight: 21, maxWidth: 25}}
              source={
                focused
                  ? require('./calendar-active.png')
                  : require('./calendar-inactive.png')
              }
              height={21}
              width={25}
            />
          ),
        }}
      />
      <Tab.Screen
        name="More"
        component={MoreScreen}
        options={{
          tabBarActiveTintColor: ThemeColor.deepGreen,
          tabBarLabelStyle: {fontFamily: 'Rubik-SemiBold', fontSize: 10},
          tabBarIcon: ({focused}) => (
            <Image
              source={
                focused
                  ? require('./more-active.png')
                  : require('./more-inactive.png')
              }
              style={{maxHeight: 20, maxWidth: 25}}
              height={20}
              width={25}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default function Home({deviceId}) {
  const numMfdPerWeek = 2;
  const [user, setUser] = useState<any>({});
  const [mfds, setMfds] = useState<any[]>([]);
  const [tempMfds, setTempMfds] = useState<any[]>([]);
  const [currentWeek, setCurrentWeek] = useState<number>(-1);
  const [addedMfdToday, setAddedMfdToday] = useState<boolean>(false);
  const [startDate, setStartDate] = useState<Date | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => deviceId && loadData(), [deviceId]);

  const toggleMfd = (date?: Moment.Moment) => {
    const dateToToggle = date?.isValid() ? Moment(date) : Moment();
    //console.log('Toggling ' + dateToToggle.format());
    const startOfDay = Moment(dateToToggle).startOf('day');
    const endOfDay = Moment(dateToToggle).endOf('day');
    const mfd = mfds.find(mfd => {
      const currDate = Moment(mfd.mfdDate);
      return currDate.isBetween(startOfDay, endOfDay, null, '[]');
    });
    if (!!mfd) {
      // Mfd already exists - remove it
      deleteMfd(dateToToggle);
    } else {
      // Mfd doesn't exist - add it
      addMfd(Moment(dateToToggle));
    }
  };

  const loadUser = async () => {
    return await fetch(`${API_URL}/user-device/${deviceId}`, {
      method: 'GET',
      headers: JSON_RESPONSE,
    })
      .then(response => response.json())
      .then(user => {
        const newUser = {...user};
        setUser(newUser);
        const startOfRegoDay = Moment(user.createdDate).startOf('day');
        setStartDate(new Date(startOfRegoDay.format()));
        setCurrentWeek(Moment().diff(startOfRegoDay, 'week'));
        return user;
      })
      .catch(error => console.error(error));
  };

  const loadMfds = async () => {
    return await fetch(`${API_URL}/mfd/${deviceId}`, {
      method: 'GET',
      headers: JSON_RESPONSE,
    })
      .then(response => response.json())
      .then(mfds => {
        setMfds([...mfds]);
        const startOfDay = Moment().startOf('day');
        const mfd = mfds.find(m => Moment(m.mfdDate).isSameOrAfter(startOfDay));
        setAddedMfdToday(!!mfd);
        return mfds;
      })
      .catch(error => console.error(error));
  };

  const loadData = async () => {
    setLoading(true);
    try {
      await Promise.all([loadUser(), loadMfds()]).then(() => setLoading(false));
    } catch (error) {
      // console.log(error);
      setLoading(false);
    }
  };

  const addMfd = async (date: Moment.Moment) => {
    // console.log('Adding MFD for ' + date.format());
    if (mfdExists(date)) {
      // The user can't add multiple MFDs for the same day
      return;
    }
    // Add a temp MFD so that the user gets instant feedback
    addTempMfdToState(date);
    try {
      await fetch(`${API_URL}/mfd/${deviceId}`, {
        method: 'POST',
        headers: JSON_RESPONSE,
        body: JSON.stringify({date: date.format()}),
      })
        .then(response => response.json())
        .then(mfd => {
          // Remove the temp MFD and add the mfd returned from the server
          removeTempMfdFromState(date);
          // Remove the temporary mfd added to the array
          addMfdToState(mfd);
        })
        .catch(error => {
          console.log(error);
          // Call failed - so we remove the temp MFD from the state
          removeTempMfdFromState(date);
        });
    } catch (error) {
      // A critical error occurred - remove temp MFD from the state
      console.log(error);
      removeTempMfdFromState(date);
    }
  };

  const deleteMfd = async (date: Moment.Moment) => {
    const start = Moment(date).startOf('day');
    const end = Moment(date).endOf('day');
    let mfdToRemove = mfds.find(m =>
      Moment(m.mfdDate).isBetween(start, end, null, '[]'),
    );
    if (!mfdToRemove) {
      // The mfd is either temp, or non-existent so we ignore the request
      return;
    }
    mfdToRemove = Object.assign({}, mfdToRemove);
    // Remove the mfd from the state to provide immediate feedback
    removeMfdFromState(mfdToRemove);
    // console.log('mfd to remove', mfdToRemove);
    try {
      await fetch(`${API_URL}/mfd/${mfdToRemove.id}`, {
        method: 'DELETE',
        headers: JSON_RESPONSE,
      })
        .then(response => {
          if (!response.ok) {
            // Request failed - add the MFD back to the state
            addMfdToState(mfdToRemove);
          }
        })
        .catch(error => {
          console.log(error);
          // Request failed - add the MFD back to the state
          addMfdToState(mfdToRemove);
        });
    } catch (error) {
      // Request critically failed - add the MFD back to the state
      console.log(error);
      addMfdToState(mfdToRemove);
    }
  };

  const addMfdToState = (mfd: any): void => {
    if (isDateToday(Moment(mfd.mfdDate))) {
      setAddedMfdToday(true);
    }
    // console.log('Adding mfd for date ' + mfd.mfdDate);
    const newMfds = [...mfds, mfd];
    setMfds(newMfds);
  };

  const removeMfdFromState = (mfd: number) => {
    // console.log('Removing mfd for date ' + mfd.mfdDate);
    const newMfds = [...mfds];
    const removeIndex = newMfds.findIndex(m => m.id == mfd.id);
    if (removeIndex != -1) {
      const mfd = newMfds.splice(removeIndex, 1);
      if (isDateToday(Moment(mfd[0].mfdDate))) {
        setAddedMfdToday(false);
      }
      setMfds(newMfds);
    }
  };

  const addTempMfdToState = (date: Moment.Moment) => {
    if (isDateToday(date)) {
      setAddedMfdToday(true);
    }
    // console.log('Adding temp mfd for ' + date?.format());
    if (!date) {
      date = Moment();
    }
    const isToday = isSameDay(date, Moment());
    if (isToday && addedMfdToday) {
      // The user is unable to add a second mfd entry for the same day
      return;
    }
    const temp = [...tempMfds, {mfdDate: date.format()}];
    setTempMfds(temp);
  };

  const removeTempMfdFromState = (date: Moment.Moment) => {
    if (isDateToday(date)) {
      setAddedMfdToday(false);
    }
    // console.log('Removing temp mfd for date ' + date.format());
    const temp = [...tempMfds];
    const start = Moment(date).startOf('day');
    const end = Moment(date).endOf('day');
    const index = temp.findIndex(m => Moment(m.mfdDate).isBetween(start, end, null, '[]'));
    if (index != -1) {
      temp.splice(index, 1);
    }
    setTempMfds(temp);
  };

  const isDateToday = (date: Moment.Moment): boolean => {
    const startOfToday = Moment().startOf('day');
    const endOfToday = Moment().endOf('day');
    return date.isBetween(startOfToday, endOfToday, null, '[]');
  };

  const mfdExists = (date: Moment.Moment) => {
    const startOfDate = Moment(date).startOf('day');
    const endOfDate = Moment(date).endOf('day');
    const mfdIsInWeek = (mfd: any) => {
      const d = Moment(mfd.mfdDate);
      return d.isBetween(startOfDate, endOfDate, null, '[]');
    };
    const mfd = mfds.find(m => mfdIsInWeek(m));
    const tempMfd = mfds.find(m => mfdIsInWeek(m));
    return !!mfd || !!tempMfd;
  };

  return (
    <NavigationContainer>
      <StatusBar barStyle={'dark-content'} />
      <MyTabs
        mfds={mfds}
        tempMfds={tempMfds}
        toggleMfd={(date) => toggleMfd(date)}
        numMfdPerWeek={numMfdPerWeek}
        addMfdToday={addedMfdToday}
        currentWeek={currentWeek}
        deviceId={deviceId}
        startDate={startDate}
      />
    </NavigationContainer>
  );
}
