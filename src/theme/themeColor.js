const ThemeColor = {
  green: '#67BB56',
  pink: '#E188B6',
  coral: '#F96A62',
  deepGreen: '#153E3A',
  lightGrey: '#CCCCCC',
  grey: '#888889',
  border: '#E0E0E0',
};

export default ThemeColor;
