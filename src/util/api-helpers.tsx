export const API_URL = 'https://dctestapp-server.digitalpurpose.com.au/api/mobile';

export const JSON_RESPONSE = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
};
