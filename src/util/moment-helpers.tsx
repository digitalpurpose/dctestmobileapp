import Moment from "moment";

export const isSameDay = (m1: Moment.Moment, m2: Moment.Moment) => {
  return (
    m1.isValid() &&
    m2.isValid() &&
    m1.get('date') == m2.get('date') &&
    m1.get('month') == m2.get('month') &&
    m1.get('year') == m2.get('year')
  );
};
